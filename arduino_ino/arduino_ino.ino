//////////////////////////////////////////////////////////////////
//©2011 bildr
//Released under the MIT License - Please reuse change and share
//Simple code for the ADXL335, prints calculated orientation via serial
//////////////////////////////////////////////////////////////////


//Analog read pins
const int pPin = 0;
const int xPin = 1;
const int yPin = 2;
const int zPin = 3;


//The minimum and maximum values that came from
//the accelerometer while standing still
//You very well may need to change these
int minVal = 265;
int maxVal = 402;

boolean sync = false;

//to hold the caculated values
double x;
double y;
double z;
double pres;


void setup(){
  Serial.begin(9600); 
}

void(* resetFunc) (void) = 0; //declare reset function @ address 0

void loop(){

  //read the analog values from the accelerometer
  int xRead = analogRead(xPin);
  int yRead = analogRead(yPin);
  int zRead = analogRead(zPin);
  int pres = analogRead(pPin);

  //convert read values to degrees -90 to 90 - Needed for atan2
  int xAng = map(xRead, minVal, maxVal, -90, 90);
  int yAng = map(yRead, minVal, maxVal, -90, 90);
  int zAng = map(zRead, minVal, maxVal, -90, 90);

  //Caculate 360deg values like so: atan2(-yAng, -zAng)
  //atan2 outputs the value of -π to π (radians)
  //We are then converting the radians to degrees
  x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
  y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
  z = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);

//    Serial.print("{ ");
//    Serial.print("\'x\': ");
//    Serial.print(x);
//    Serial.print(",\'y\': ");
//    Serial.print(y);
//    Serial.print(",\'z\': ");
//    Serial.print(z);
//    Serial.print(", \'p\': ");
//    Serial.print(pres);
//    Serial.println("}");
  String dict = "{";
    dict = dict + "\'x\': ";
    dict = dict +  String(int(x));
    dict = dict +  ",\'y\': ";
    dict = dict +  String(int(y));
    dict = dict +  ",\'z\': ";
    dict = dict +  String(int(z));
    dict = dict +  ", \'p\': ";
    dict = dict +  String(int(pres));
    dict = dict +  "}";
    Serial.println(dict);

  delay(20);//just here to slow down the serial output - Easier to read
}
