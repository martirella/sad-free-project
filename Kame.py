#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Módulos
import sys,pygame, math, serial, random, ast
from pygame.locals import *

#Constantes
WIDTH = 640
HEIGHT = 480

#Clases
#----------------------------
class Player(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = load_image('Images/GOKU.png', True)
        self.rect = self.image.get_rect()
        self.rect.centerx = 40
        self.rect.centery = HEIGHT - 140
        self.angle = 0
        self.pressio = 0
        #self.speed = [0.5, 0.1]
        self.life= 100
        self.charge = 50
        self.kames = []
        self.cooldown = -1
        self.score = 0

    def draw(self, screen, time, keys, d):

        angle = d['y']
        press = d['p']

        screen.blit(self.image, self.rect)
        self.update(angle, 0)
        self.show_life(screen)
        self.show_score(screen)

        if self.cooldown > 0:
            self.cooldown -= time
        else:
            #self.disparar(keys, angle)
            self.dispararPress(angle, press)

        for k in self.kames:
            if k.out():
                self.kames.remove(k)
            else:
                k.draw(screen, time)

    def update(self, angle, pressio):
        self.angle = -1*angle
        self.pressio = pressio

    def show_life(self, screen):
        color = (0,0,0)
        if self.life > 70:
            color = (0,255,0)
        elif self.life > 30:
            color = (255,255,0)
        else:
            color = (255, 0, 0)

        life = pygame.Rect(30, 30, 2*self.life, 30)
        pygame.draw.rect(screen, color, life)
        box = pygame.Rect(30, 30, 200, 30)
        pygame.draw.rect(screen, (0, 0, 0), box, 3)

    def show_score(self, screen):
        t_score, t_score_rect = texto("SCORE: " + str(self.score), 320 ,50 )
        screen.blit(t_score, t_score_rect)

    def dispararPress(self, angle, press):
        if press > 230:
            self.kames.append(Kame(self.rect.centerx, self.rect.centery, self.angle, (press-200)/30))
            self.cooldown = 200

class Kame(pygame.sprite.Sprite):

    def __init__(self, x, y, angle, scale):
        pygame.sprite.Sprite.__init__(self)
        self.image0 = load_image('Images/KAME.png', True)
        self.image = pygame.transform.scale(self.image0, (self.image0.get_width()*scale, self.image0.get_height()*scale) )
        self.image = pygame.transform.rotate(self.image, -1*angle)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.centery = y
        self.angle = angle
        self.velocitat = (math.cos(math.radians(angle)), math.sin(math.radians(angle)))


    def update(self, time):
        self.rect.centerx += time*self.velocitat[0]
        self.rect.centery += time*self.velocitat[1]

    def draw(self, screen, time):
        self.update(time)
        screen.blit(self.image, self.rect)

    def out(self):
        if self.rect.left < 0:
            return True
        if self.rect.right > WIDTH:
            return True
        if self.rect.top < 0:
            return True
        if self.rect.bottom > HEIGHT:
            return True
        return False

class Enemy(object):
    def __init__(self):
        self.enemies = []

    def draw(self, screen, time, playery):
        self.atacar(playery)

        for e in self.enemies:
            if e.out():
                self.enemies.remove(e)
            else:
                e.draw(screen, time)

    def atacar(self, playery):
        rand = random.randint(0,10)
        if rand < 1:
            self.enemies.append(BadKame(playery))

class BadKame(pygame.sprite.Sprite):

    def __init__(self, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = load_image('Images/BADKAME.png', True)
        self.rect = self.image.get_rect()
        self.rect.centerx = WIDTH
        self.rect.centery = random.randint(y-50,y+50)
        self.velocitat = -0.5
        self.damage = 10

    def update(self, time):
        self.rect.centerx += time*self.velocitat

    def draw(self, screen, time):
        self.update(time)
        screen.blit(self.image, self.rect)

    def out(self):
        if self.rect.left < 0:
            return True
        return False


#----------------------------

#Funciones
#----------------------------
def load_image(filename, transparent=False):
    try: image = pygame.image.load(filename)
    except pygame.error, message:
        raise SystemExit,message
    image = image.convert()
    if transparent:
        color = image.get_at((0,0))
        image.set_colorkey(color, RLEACCEL)
    return image

def texto(texto, posx, posy, color=(0, 0, 0), size=25, font='Fonts/DroidSans.ttf'):
    fuente = pygame.font.Font(font, size)
    #fuente = pygame.font.Font('Fonts/Saiyan-Sans.ttf', 25)
    salida = pygame.font.Font.render(fuente, texto, 1, color)
    salida_rect = salida.get_rect()
    salida_rect.centerx = posx
    salida_rect.centery = posy
    return salida, salida_rect

def colisions(player, enemic):
    #Colisio badkame i goku
    for e in enemic.enemies:
        if pygame.sprite.collide_rect(player,e):
            player.life -= e.damage
            enemic.enemies.remove(e)

    #Colisio badkame i kame
    for k in player.kames:
        for e in enemic.enemies:
            if pygame.sprite.collide_rect(e,k):
                enemic.enemies.remove(e)
                player.kames.remove(k)
                player.score += 1

#----------------------------

def main():
    screen = pygame.display.set_mode((WIDTH, HEIGHT), pygame.FULLSCREEN)   #Creamos laventana con dimensiones Width x Height
    #screen = pygame.display.set_mode((WIDTH, HEIGHT))   #Creamos laventana con dimensiones Width x Height
    pygame.display.set_caption("Kame!")

    sensor = serial.Serial(sys.argv[1], 9600)

    background_image = load_image('Images/DESERT8BITS.png')

    clock = pygame.time.Clock()
    keys = pygame.key.get_pressed()

    goku = Player()
    enemy = Enemy()

    debug = False
    alive = True
    while not keys[K_ESCAPE]:
        time = clock.tick(60)
        keys = pygame.key.get_pressed()
        read = sensor.readline()
        #print read
        try:
            sensorRead = ast.literal_eval(read)
        except SyntaxError:
            sensorRead = {'x':0, 'y': 0, 'z':0, 'p':0}
        for eventos in pygame.event.get():              #Miramos si la ventana se ha cerrado
            if eventos.type == QUIT:
                sys.exit(0)

        if keys[K_F12]:
            debug = True
        if keys[K_F11]:
            debug = False

        if keys[K_m]:
            goku.life -= 10

        screen.blit(background_image, (0,0))

        if alive:
            goku.draw(screen, time, keys, sensorRead)
            #goku.drawk(screen, time, keys)
            enemy.draw(screen, time, goku.rect.centery)
            colisions(goku, enemy)

            if debug:
                kame_count, kame_count_rect = texto(str(len(goku.kames)), WIDTH-50 , 20)
                screen.blit(kame_count,kame_count_rect)
                t_angle,t_angle_rect = texto(str(goku.angle), WIDTH-50, 50)
                screen.blit(t_angle, t_angle_rect)
                pygame.draw.line(screen, (0, 0, 255), (goku.rect.centerx, goku.rect.centery), (goku.rect.centerx+math.cos(math.radians(goku.angle))*100, goku.rect.centery+math.sin(math.radians(goku.angle))*100))
                t_life, t_life_rect = texto(str(goku.life), WIDTH-50, 80, (255, 0, 0) )
                screen.blit(t_life, t_life_rect)
                t_press, t_press_rect = texto(str(sensorRead['p']), WIDTH-50, 110, (255, 0, 0) )
                screen.blit(t_press, t_press_rect)

        if not alive:
            t_score, t_score_rect = texto("SCORE:" + str(goku.score), WIDTH/2, HEIGHT/4, (0, 0, 0), 60)
            screen.blit(t_score, t_score_rect)
            t_dead, t_dead_rect = texto("Has mort", WIDTH/2, HEIGHT/2, (0, 0, 0), 100, 'Fonts/Saiyan-Sans.ttf' )
            screen.blit(t_dead, t_dead_rect)
            t_restart, t_restart_rect = texto("Prem espai per reviure", WIDTH/2, HEIGHT/2+100, (0, 0, 0), 30, 'Fonts/Saiyan-Sans.ttf' )
            screen.blit(t_restart, t_restart_rect)
            if keys[K_SPACE]:
                goku = Player()
                alive = True

        if goku.life <= 0:
            alive = False

        pygame.display.flip()
    return 0

if __name__ == '__main__':
    pygame.init()
    main()
